[![Travis CI](https://travis-ci.org/Jaesin/autovalue.svg?branch=8.x-1.x)](https://travis-ci.org/Jaesin/autovalue)

Automatic Value
---------------

#### Provides an api for providing auto populated values.

Plugin concept inspired by the entity labels sandbox project: [Benedikt entity_labels](https://www.drupal.org/sandbox/bforchhammer/2278229).
